package dip;

public interface IOfficeUser {
    void publishNewPost(String postMessage);
}
