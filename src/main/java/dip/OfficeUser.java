package dip;

import openclosed.ex0.Post;

public class OfficeUser implements IOfficeUser{
    private Post post;

   //get set Post

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    @Override
    public void publishNewPost(String postMessage) {
        postMessage = "example message";
        post.createPost(postMessage);
    }
}
