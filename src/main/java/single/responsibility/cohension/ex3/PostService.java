package single.responsibility.cohension.ex3;

public interface PostService {
    void CreatePost(String postMessage);
}
