package single.responsibility.cohension.ex3;

public class PostServiceImpl implements PostService {
    private DAO db;
    private Util util;

    public DAO getDb() {
        return db;
    }

    public void setDb(DAO db) {
        this.db = db;
    }

    public Util getUtil() {
        return util;
    }

    public void setUtil(Util util) {
        this.util = util;
    }

    public void CreatePost(String postMessage) {
        try {
            db.Add(postMessage);
        } catch (Exception ex) {
            util.logError("An error occured: ",ex.toString());
            util.writeAllText("LocalErrors.txt", ex.toString());
        }
    }
}