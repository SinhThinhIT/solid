package single.responsibility.cohension.ex3;

public interface DAO {
    public void Add(String postMessage);
    public void addAsTag(String postMessage);
    public void addAsMentionPost(String postMessage);
}
