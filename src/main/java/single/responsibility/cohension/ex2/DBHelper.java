package single.responsibility.cohension.ex2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBHelper {
    //connection
    public Connection getConnection() {
        try {
            return DriverManager.getConnection("jdbc:msql://xxx.yyyy.zzz.lll:3306/Demo", "", "");
        } catch (SQLException ex) {
            System.err.println("Erro: " + ex);
            return null;
        }
    }
}
