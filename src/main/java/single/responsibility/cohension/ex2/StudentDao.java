package single.responsibility.cohension.ex2;

public interface StudentDao {
    public void saveOrUpdate(Student student);
}
