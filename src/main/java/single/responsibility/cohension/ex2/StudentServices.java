package single.responsibility.cohension.ex2;

public interface StudentServices {
    void saveToDatabase(Student student);
}
