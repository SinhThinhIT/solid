package single.responsibility.cohension.ex2;

public class StudentServiceImpl implements StudentServices{
    //call to daoImpl
    private StudentDao studentDao;

    public StudentDao getStudentDao() {
        return studentDao;
    }

    public void setStudentDao(StudentDao studentDao) {
        this.studentDao = studentDao;
    }

    @Override
    public void saveToDatabase(Student student) {
        studentDao.saveOrUpdate(student);
    }
}
