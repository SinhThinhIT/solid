package single.responsibility.cohension.ex2;

import java.beans.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class StudentDaoImpl implements StudentDao {
    private DBHelper dbHelper;

    public StudentDaoImpl() {
    }


    @Override
    public void saveOrUpdate(Student student) {
        String sql = "INSERT INTO Student(id,name,age) " +
                "VALUES (?, ?, ?)";
        try {
            Connection con = dbHelper.getConnection();
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, student.getId());
            stmt.setString(2, student.getName());
            stmt.setInt(3, student.getAge());
            stmt.execute();
        }catch (SQLException e){
            System.out.println("Error: " + e);
        }
    }

    public DBHelper getDbHelper() {
        return dbHelper;
    }

    public void setDbHelper(DBHelper dbHelper) {
        this.dbHelper = dbHelper;
    }
}
