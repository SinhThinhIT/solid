package single.responsibility.cohension.ex1;

public class SquareImpl implements ISquare{

    @Override
    public int calculateP(Square square) {
        return square.getSize() * 4;
    }

    @Override
    public int calculateArea(Square square) {
        return square.getSize() * square.getSize();
    }

    @Override
    public void draw() {
        System.out.println("draw square now!!");
    }

    @Override
    public void rotate() {
        System.out.printf("we rotate this square now!!");
    }


}
