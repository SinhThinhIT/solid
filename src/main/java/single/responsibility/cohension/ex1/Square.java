package single.responsibility.cohension.ex1;

class Square {
    private int size;
    public Square(int size) {
        this.size = size;
    }
    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
