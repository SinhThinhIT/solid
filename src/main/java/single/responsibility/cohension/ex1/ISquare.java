package single.responsibility.cohension.ex1;

interface ISquare {
    int calculateP(Square square);
    int calculateArea(Square square);
    public void draw();
    public void rotate();
}
