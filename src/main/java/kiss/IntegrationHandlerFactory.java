package kiss;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class IntegrationHandlerFactory {

    private final EmailIntegrationHandler emailHandler;
    private final SMSIntegrationHandler smsHandler;
    private final PushIntegrationHandler pushHandler;

    public IntegrationHandlerFactory(EmailIntegrationHandler emailHandler,
                                     SMSIntegrationHandler smsHandler,
                                     PushIntegrationHandler pushHandler) {
        this.emailHandler = emailHandler;
        this.smsHandler = smsHandler;
        this.pushHandler = pushHandler;
    }

    public IntegrationHandler getHandlerFor(String integration)  {
        Map<String, String> map = new HashMap<>();
        map.put("abc", "emaiHander");
        map.put("dfe", "sms");
        map.put("glk", "push");
        Set<String> set = map.keySet();
        for (String key : set){
            if (key.equals(integration)){
                System.out.println(map.get(key));
            }else {
                throw new IllegalArgumentException("No handler found for integration: " + integration);
            }
        }
        return null;
    }
}