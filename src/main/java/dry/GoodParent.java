package dry;

import java.util.ArrayList;
import java.util.List;

public class GoodParent {
    public List<Person> findAllGoodPerson(List<Person> persons, String gender) {
        persons = new ArrayList<Person>();
        for (Person person : persons) {
            if (person.name.startsWith("T")
                    && person.age > 25
                    && person.hairColor == HairColor.BLACK
                    && person.skinColor == SkinColor.WHITE
            ) {
                if (person.sex == Gender.MALE && gender.equals(Gender.MALE)) {
                    persons.add(person);
                }else{
                    persons.add(person);
                }
            }

        }

        return persons;
    }
}

class Person {
    String name;
    int age;
    Gender sex;
    HairColor hairColor;
    SkinColor skinColor;


}

enum SkinColor {
    BLACK, BROWN, WHITE
}

enum HairColor {
    RED, BLACK, YELLOW
}

enum Gender {
    FEMALE,
    MALE,
    GAY,
    LES
}


